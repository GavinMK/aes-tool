import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class AESEncrypt128 {
    public static void main(String[] args) {

        if(args.length != 1) {
            throw new IllegalArgumentException("Malformed arguments. Use java AESEncrypt128 <absolute_file_path>");
        }

        String filepath = args[0];

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a 32 digit hex value");
        String inputkey = scanner.nextLine().replaceAll("\\s+", "");
        scanner.close();

        Data128 key = new Data128(inputkey);
        AES encryptor = new AES();
        Data128[] key_schedule = encryptor.getKeySchedule(key);

        FileInputStream fs;
        FileOutputStream fos;
        try {
            fs = new FileInputStream(filepath);
            fos = new FileOutputStream(filepath.substring(0, filepath.indexOf('.')) + ".enc");
            byte[] b = new byte[16];
            Data128 ciphertext;
            while(fs.read(b, 0, 16) != -1){
                ciphertext = new Data128(b);
                ciphertext = encryptor.keyAddition(key_schedule[0], ciphertext);

                for(int i = 1; i < 10; i++){
                    ciphertext = encryptor.byteSubstitution(ciphertext);
                    ciphertext = encryptor.shiftRow(ciphertext);
                    ciphertext = encryptor.mixCol(ciphertext);
                    ciphertext = encryptor.keyAddition(key_schedule[i], ciphertext);
                }

                ciphertext = encryptor.byteSubstitution(ciphertext);
                ciphertext = encryptor.shiftRow(ciphertext);
                ciphertext = encryptor.keyAddition(key_schedule[10], ciphertext);

                fos.write(ciphertext.getByteArray());

                b = new byte[16];
            }
            fos.close();
        }
        catch(IOException e) {
            System.out.println("File given as input does not exist.");
        }
    }
}
