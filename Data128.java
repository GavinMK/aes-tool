
public class Data128 {
    private char[] data = new char[16];

    Data128(){}

    Data128(char[] c){
        this.data = c;
    }

    Data128(String key){
        for(int i = 0; i < 32; i+=2){
            String temp = key.substring(i, i+2);
            this.data[i / 2] = (char) (Integer.parseInt(temp, 16) & 0x00FF);
        }
    }

    Data128(int a, int b, int c, int d){
        for(int i = 0; i < 4; i++){
            this.data[i] = (char) ((a >> (3 - i) * 8) & 0x00FF);
        }
        for (int i = 0; i < 4; i++){
            this.data[i + 4] = (char) ((b >> (3 - i) * 8) & 0x00FF);
        }
        for (int i = 0; i < 4; i++){
            this.data[i + 8] = (char) ((c >> (3 - i) * 8) & 0x00FF);
        }
        for (int i = 0; i < 4; i++){
            this.data[i + 12] = (char) ((d >> (3 - i) * 8) & 0x00FF);
        }
    }

    Data128(byte[] b){
        for(int i = 0; i < 16; i++){
            this.data[i] = (char) (b[i] & 0x00FF);
        }
    }

    public Data128 xorWith(Data128 d){
        Data128 res = new Data128();
        for(int i = 0; i < 16; i++){
            res.data[i] = (char) ((this.data[i] ^ d.data[i]) & 0x00FF);
        }
        return res;
    }

    public char getByte(int index){
        return this.data[index];
    }

    public byte[] getByteArray(){
        byte[] b = new byte[16];
        for(int i = 0; i < 16; i++){
            b[i] = (byte) (this.data[i] & 0x00FF);
        }
        return b;
    }

    public int getWord(int index){
        int word = 0;
        for(int i = 0; i < 4; i++){
            word += ((int) (this.data[i + (4 * index)]) << (3 - i) * 8);
        }
        return word;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (char b : this.data) {
            builder.append(String.format("%02X", (int) b));
        }
        return builder.toString();
    }
}
